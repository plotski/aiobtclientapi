import re

import pytest

from aiobtclientapi.torrentdata import types


@pytest.mark.parametrize(
    argnames='value, exp_result',
    argvalues=(
        ('0123456789abcdef0123456789abcdef01234567', '0123456789abcdef0123456789abcdef01234567'),
        ('0123456789ABCDEF0123456789ABCDEF01234567', '0123456789abcdef0123456789abcdef01234567'),
        ('0123456789ABCDEF0123456789abcdeF01234567', '0123456789abcdef0123456789abcdef01234567'),
        ('0123456789ABCDEF0123456789abcdeF0123456x', ValueError("Invalid infohash: '0123456789ABCDEF0123456789abcdeF0123456x'")),
        ('0123456789ABCDEF0123456789abcdeF0123456x', ValueError("Invalid infohash: '0123456789ABCDEF0123456789abcdeF0123456x'")),
        (None, ValueError('Invalid infohash: None')),
    ),
    ids=lambda v: repr(v),
)
def test_Infohash(value, exp_result):
    if isinstance(exp_result, Exception):
        with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_result))}$'):
            types.Infohash(value)
    else:
        result = types.Infohash(value)
        assert result == exp_result
        assert str(result) == str(result).lower()
        assert hash(result) == hash(result)
        assert hash(result) != hash(types.Infohash('f00000000000000000000000000000000000000f'))


@pytest.mark.parametrize(
    argnames='infohash, other, exp_return_value',
    argvalues=(
        (types.Infohash('0123456789abcdef0123456789abcdef01234567'), '0123456789abcdef0123456789abcdef01234568', False),
        (types.Infohash('0123456789abcdef0123456789abcdef01234567'), '0123456789abcdef0123456789abcdef012345677', False),
        (types.Infohash('0123456789abcdef0123456789abcdef01234567'), '0123456789abcdef0123456789abcdef01234567', True),
        (types.Infohash('0123456789abcdef0123456789abcdef01234567'), '0123456789ABCDEF0123456789abcdef01234567', True),
        (types.Infohash('0123456789abcdef0123456789ABCDEF01234567'), '0123456789abcdef0123456789ABCDEF01234567', True),
        (types.Infohash('0123456789abcdef0123456789ABCDEF01234567'), '0123456789abcdef0123456789ABCDEF01234567', True),
        (types.Infohash('0123456789abcdef0123456789ABCDEF01234567'), 123, NotImplemented),
    ),
    ids=lambda v: repr(v),
)
def test_infohash_equality(infohash, other, exp_return_value):
    assert infohash.__eq__(other) is exp_return_value


@pytest.mark.parametrize(
    argnames='value, exp_string',
    argvalues=(
        (types.Ratio.NOT_APPLICABLE, ''),
        (types.Ratio.INFINITE, '∞'),
        (1.23456, '1.23456'),
    ),
)
def test_Ratio(value, exp_string):
    ratio = types.Ratio(value)
    assert ratio == value
    assert str(ratio) == exp_string
