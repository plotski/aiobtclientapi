Reference
=========

.. automodule:: aiobtclientapi
   :members:

.. autosummary::
   :toctree:
   :template: autosummary-template.rst
   :nosignatures:

   aiobtclientapi.clients
   aiobtclientapi.constants
   aiobtclientapi.torrentdata
   aiobtclientapi.torrentdata.attrs
   aiobtclientapi.torrentdata.types
   aiobtclientapi.utils
   aiobtclientapi.utils.constant
   aiobtclientapi.errors
