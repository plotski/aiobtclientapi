#!/bin/bash

set -o nounset   # Don't allow unset variables
set -o errexit   # Exit if any command fails

homedir="$(realpath "$(dirname "$0")")"
projectdir="$(realpath "$homedir/../..")"
workdir=/tmp/qbittorrent.aiobtclientrpc
contentdir="$workdir/contents/"
torrentdir="$workdir/torrents/"
downloaddir="$workdir/downloads"
watchdir="$workdir/add_torrents"

source "$(dirname "$homedir")/rpc-connections.env"
source "$(dirname "$homedir")/utils.sh"
source "$(dirname "$homedir")/common-server.sh"

mkdir -p "$workdir/qBittorrent/config/"
mkdir -p "$watchdir"

cleanup() {
    rm -r "$workdir"
}

trap cleanup EXIT


cat <<-EOF > "$workdir/qBittorrent/config/qBittorrent.conf"
[Preferences]
Bittorrent\DHT=false
Connection\UPnP=false
Downloads\ScanDirsLastPath=$watchdir
WebUI\Address=$QBITTORRENT_RPC_HOST
WebUI\Enabled=true
WebUI\Port=$QBITTORRENT_RPC_PORT
WebUI\LocalHostAuth=false
WebUI\Password_PBKDF2="@ByteArray(uVV2iUo6AcGggiCpOw+tgQ==:tY8MbzaxLBFFN8zPvdzEPXjhXlKMCA3Qw77GXcWAU526C8mfijPE15D2L0alI2z2CRfPj75id+awmg2mLeioNA==)"

[Network]
PortForwardingEnabled=false

[BitTorrent]
Session\DHTEnabled=false
Session\DefaultSavePath=$downloaddir

[LegalNotice]
Accepted=true
EOF

cat <<-EOF > "$workdir/qBittorrent/config/watched_folders.json"
{
    "$watchdir": {
        "add_torrent_params": {
            "category": "",
            "content_layout": "Original",
            "download_limit": -1,
            "download_path": "$downloaddir/temp",
            "operating_mode": "AutoManaged",
            "ratio_limit": -2,
            "save_path": "$downloaddir",
            "seeding_time_limit": -2,
            "skip_checking": true,
            "stopped": true,
            "tags": [
            ],
            "upload_limit": -1,
            "use_auto_tmm": false,
            "use_download_path": false
        },
        "recursive": false
    }
}
EOF


(
    sleep 1
    create_and_add_torrents_via_watchdir "$torrentdir" "$contentdir" "$downloaddir" "$CLI_ADD_TORRENTS" "$watchdir"
) &

qbittorrent --profile="$workdir"
