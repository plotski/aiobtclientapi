#!/bin/bash

set -o nounset   # Don't allow unset variables
set -o errexit   # Exit if any command fails

homedir="$(realpath "$(dirname "$0")")"
projectdir="$(realpath "$homedir/../..")"
workdir=/tmp/transmission.aiobtclientrpc
contentdir="$workdir/contents/"
torrentdir="$workdir/torrents/"
downloaddir="$workdir/downloads/"
watchdir="$workdir/watch"

source "$(dirname "$homedir")/rpc-connections.env"
source "$(dirname "$homedir")/utils.sh"
source "$(dirname "$homedir")/common-server.sh"

mkdir -p "$workdir"
mkdir -p "$watchdir"
mkdir -p "$downloaddir"

cleanup() {
    [ -f "$workdir/pid" ] && (
        kill "$(cat "$workdir/pid")"
        echo -n 'Waiting for transmission-daemon to exit'
        wait_for_file_to_disappear "$workdir/pid"
    )
    rm -rf "$workdir"
}

trap cleanup EXIT


run_transmission_daemon() {
    transmission-daemon \
        "$@" \
        --logfile "$workdir/log" \
        --pid-file "$workdir/pid" \
        --config-dir "$workdir/config/" \
        --download-dir "$downloaddir" \
        --watch-dir "$watchdir" \
        --auth --username "$TRANSMISSION_RPC_USERNAME" --password "$TRANSMISSION_RPC_PASSWORD" \
        --rpc-bind-address "$TRANSMISSION_RPC_HOST" --port "$TRANSMISSION_RPC_PORT" \
        --peerport 54321 --no-portmap --no-dht \
        --paused
}

stig() {
    local stig_url="$TRANSMISSION_RPC_USERNAME:$TRANSMISSION_RPC_PASSWORD@$TRANSMISSION_RPC_HOST:$TRANSMISSION_RPC_PORT"
    command stig \
        --no-rc-file \
        set tui.cli.history-dir "$workdir/stig" \
        and set connect.url "$stig_url" \
        and "$@"
}


run_transmission_daemon &

create_and_add_torrents_via_watchdir "$torrentdir" "$contentdir" "$downloaddir" "$CLI_ADD_TORRENTS" "$watchdir"

stig \
    set tui.poll 1 \
    and tab -C \
    and tab ls
