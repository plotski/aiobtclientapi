set -o nounset
set -o errexit


wait_for_file_to_disappear() {
    local filepath="$1"

    while [ -e "$workdir/pid" ]; do
        sleep 1
        echo -n '.'
    done
    echo
}


get_random_int() {
    # Random number from 1 to 255 (roughly)
    n=$(dd if=/dev/urandom bs=1 count=1 status=none | od -t d -A n | awk '{print $1}')
    echo $(( $n + 1 ))
}


get_random_word() {
    # Remove any characters that aren't lowercase letters
    shuf /usr/share/dict/words | grep -Pv '[^a-zA-Z]+' | head -n1
}


get_random_filesize() {
    if [ $(( $(get_random_int) % 4 )) = 0 ] ; then
        echo "$(( $(get_random_int) * 1000 ))"
    elif [ $(( $(get_random_int) % 3 )) = 0 ] ; then
        echo "$(( $(get_random_int) * 100 ))"
    elif [ $(( $(get_random_int) % 2 )) = 0 ] ; then
        echo "$(( $(get_random_int) * 10 ))"
    else
        get_random_int
    fi
}


get_content_path() {
    local contentdir="$1"
    echo "$contentdir"/"$(get_random_word)_$(get_random_word)_$(get_random_word)"
}


create_content_file() {
    local contentpath="$1"
    local contentsize="$2"
    dd if=/dev/urandom of="$contentpath" bs=1 count="$contentsize" status=none
}


get_singlefile_content() {
    local contentbase="$1"
    local contentpath="$(get_content_path "$contentbase")"
    create_content_file "$contentpath" "$(get_random_filesize)" \
        && echo "$contentpath"
}


create_random_files() {
    local contentpath="$1"

    mkdir -p "$contentpath"

    # Create 1 - 9 files
    local filecount="$(( $(get_random_int) / 30 + 1 ))"

    for ((i = 0 ; i < $filecount ; i++ )); do
        local filepath="$contentpath/$(get_random_word)_$(get_random_word)_$(get_random_word)"
        create_content_file "$filepath" "$(get_random_filesize)" >/dev/null
    done
}


get_multifile_content() {
    local contentbase="$1"
    local contentpath="$(get_content_path "$contentbase")"

    create_random_files "$contentpath"

    if [ $(( $(get_random_int) % 2 )) = 0 ] ; then
        local subdirpath="$contentpath/$(get_random_word)"
        create_random_files "$subdirpath"
    fi

    echo "$contentpath"
}


get_random_content() {
    local contentbase="$1"

    if [ $(( $(get_random_int) % 2 )) = 0 ] ; then
        get_singlefile_content "$contentbase"
    else
        get_multifile_content "$contentbase"
    fi
}


create_torrent() {
    local contentpath="$1"
    local torrentpath="$2"
    local trackers="$3"
    local private="$4"

    local cmd="torf --yes --out \"$torrentpath\" \"$contentpath\""

    for tracker in $trackers; do
        cmd="$cmd --tracker \"$tracker\""
    done

    if [ "$private" = 'private' ]; then
        cmd="$cmd --private"
    fi

    cmd="$cmd"
    eval "$cmd" >/dev/null
    echo "$torrentpath"
}


add_torrent_via_rpc() {
    local torrentpath="$1"
    local downloaddir="$2"
    local clientname="$3"
    local clienturl="$4"
    if [ -n "$clientname" ] && [ -n "$clienturl" ]; then
        btclient "$clientname" "$clienturl" add "=$torrentpath" "location=$downloaddir"
    fi
}


add_torrent_via_watchdir() {
    local torrentpath="$1"
    local watchdir="$2"
    if [ -d "$watchdir" ]; then
        ln -s "$torrentpath" "$watchdir"
    else
        echo "Watch directory does not exist: $watchdir"
        exit 1
    fi
}


trackers="
http://localhost:1234/announce
http://localhost:4321/announce
"

# Working trackers: https://github.com/ngosang/trackerslist
# http://bt.endpot.com:80/announce
# http://open.acgnxtracker.com:80/announce
# http://tracker.openbittorrent.com:80/announce
# https://tracker1.520.jp:443/announce
# https://tracker2.ctix.cn:443/announce
# udp://exodus.desync.com:6969/announce
# udp://explodie.org:6969/announce
# udp://open.demonii.com:1337/announce
# udp://open.stealth.si:80/announce
# udp://opentracker.i2p.rocks:6969/announce
# udp://tracker-udp.gbitt.info:80/announce
# udp://tracker.4.babico.name.tr:3131/announce
# udp://tracker.leech.ie:1337/announce
# udp://tracker.openbittorrent.com:6969/announce
# udp://tracker.opentrackr.org:1337/announce
# udp://tracker.theoks.net:6969/announce
# udp://tracker.tiny-vps.com:6969/announce
# udp://tracker1.bt.moack.co.kr:80/announce
# udp://tracker2.dler.org:80/announce
# udp://uploads.gamecoast.net:6969/announce

get_random_trackers() {
    local count="${1:-0}"

    echo "$trackers" \
        | grep -v "^$" \
        | shuf \
        | head -n "$count"
}

tag_contentpath() {
    local contentpath="$1"
    local tag="$2"

    local new_contentpath="$(dirname "$contentpath")/${tag}_$(basename "$contentpath")"

    mv "$contentpath" "$new_contentpath"
    echo "$new_contentpath"
}


seed_torrent() {
    local contentpath="$1"
    local downloaddir="$2"

    local contentpath="$(tag_contentpath "$contentpath" SEEDING)"
    ln -s "$(realpath --relative-to "$downloaddir" "$contentpath")" "$(realpath "$downloaddir")"
    echo "$contentpath"
}

create_random_torrents() {
    local torrentdir="$1"
    local contentdir="$2"
    local downloaddir="$3"
    local count="${4:-random}"

    if [ "$count" = 'random' ]; then
        count=$(( $(get_random_int) % 30 + 6 ))
    fi

    mkdir -p "$torrentdir" "$contentdir" "$downloaddir"

    for ((i = 0 ; i < $count ; i++ )); do
        # Generate everything in background process
        (
            # Generate content
            local contentpath="$(get_random_content "$contentdir")"

            # Add trackers?
            if [ $(( $(get_random_int) % 2 )) = 0 ] ; then
                local trackers="$(get_random_trackers $(( $(get_random_int) % 3 + 1 )))"
            else
                local trackers=''
            fi
            if [ -z "$trackers" ]; then
                contentpath="$(tag_contentpath "$contentpath" UNTRACKED)"
            fi

            # DHT enabled?
            if [ $(( $(get_random_int) % 2 )) = 0 ] ; then
                local private='private'
                contentpath="$(tag_contentpath "$contentpath" PRIVATE)"
            else
                local private='not_private'
            fi

            # Are we seeding the torrent?
            if [ $(( $(get_random_int) % 2 )) = 0 ] ; then
                contentpath="$(seed_torrent "$contentpath" "$downloaddir")"
            fi

            # Generate torrent
            local torrentpath="$torrentdir/$(basename "$contentpath").torrent"
            create_torrent "$contentpath" "$torrentpath" "$trackers" "$private"
        ) &
    done

    # Wait for background processes to finish
    wait
}


create_torrents() {
    local torrentdir="$1"
    local contentdir="$2"
    local downloaddir="$3"

    mkdir -p "$torrentdir" "$contentdir" "$downloaddir"

    (
        # 0 trackers, not private, not seeding
        local contentpath="$(get_random_content "$contentdir")"
        contentpath="$(tag_contentpath "$contentpath" UNTRACKED)"
        create_torrent \
            "$contentpath" \
            "$torrentdir/$(basename "$contentpath").torrent"\
            "$(get_random_trackers 0)" \
            'not_private'
    ) &

    (
        # 1 tracker, not private, not seeding
        local contentpath="$(get_random_content "$contentdir")"
        create_torrent \
            "$contentpath" \
            "$torrentdir/$(basename "$contentpath").torrent"\
            "$(get_random_trackers 1)" \
            'not_private'
    ) &

    (
        # 2 trackers, not private, not seeding
        local contentpath="$(get_random_content "$contentdir")"
        create_torrent \
            "$contentpath" \
            "$torrentdir/$(basename "$contentpath").torrent"\
            "$(get_random_trackers 2)" \
            'not_private'
    ) &


    (
        # 0 trackers, private, not seeding
        local contentpath="$(get_random_content "$contentdir")"
        contentpath="$(tag_contentpath "$contentpath" UNTRACKED)"
        contentpath="$(tag_contentpath "$contentpath" PRIVATE)"
        create_torrent \
            "$contentpath" \
            "$torrentdir/$(basename "$contentpath").torrent"\
            "$(get_random_trackers 0)" \
            'private'
    ) &

    (
        # 1 tracker, private, not seeding
        local contentpath="$(get_random_content "$contentdir")"
        contentpath="$(tag_contentpath "$contentpath" PRIVATE)"
        create_torrent \
            "$contentpath" \
            "$torrentdir/$(basename "$contentpath").torrent"\
            "$(get_random_trackers 1)" \
            'private'
    ) &

    (
        # 2 trackers, private, not seeding
        local contentpath="$(get_random_content "$contentdir")"
        contentpath="$(tag_contentpath "$contentpath" PRIVATE)"
        create_torrent \
            "$contentpath" \
            "$torrentdir/$(basename "$contentpath").torrent"\
            "$(get_random_trackers 2)" \
            'private'
    ) &


    (
        # 0 trackers, not private, seeding
        local contentpath="$(get_random_content "$contentdir")"
        contentpath="$(tag_contentpath "$contentpath" UNTRACKED)"
        contentpath="$(seed_torrent "$contentpath" "$downloaddir")"
        create_torrent \
            "$contentpath" \
            "$torrentdir/$(basename "$contentpath").torrent"\
            "$(get_random_trackers 0)" \
            'not_private'
    ) &

    (
        # 1 tracker, not private, seeding
        local contentpath="$(get_random_content "$contentdir")"
        contentpath="$(seed_torrent "$contentpath" "$downloaddir")"
        create_torrent \
            "$contentpath" \
            "$torrentdir/$(basename "$contentpath").torrent"\
            "$(get_random_trackers 1)" \
            'not_private'
    ) &

    (
        # 2 trackers, not private, seeding
        local contentpath="$(get_random_content "$contentdir")"
        contentpath="$(seed_torrent "$contentpath" "$downloaddir")"
        create_torrent \
            "$contentpath" \
            "$torrentdir/$(basename "$contentpath").torrent"\
            "$(get_random_trackers 2)" \
            'not_private'
    ) &


    (
        # 0 trackers, private, seeding
        local contentpath="$(get_random_content "$contentdir")"
        contentpath="$(tag_contentpath "$contentpath" UNTRACKED)"
        contentpath="$(tag_contentpath "$contentpath" PRIVATE)"
        contentpath="$(seed_torrent "$contentpath" "$downloaddir")"
        create_torrent \
            "$contentpath" \
            "$torrentdir/$(basename "$contentpath").torrent"\
            "$(get_random_trackers 0)" \
            'private'
    ) &

    (
        # 1 tracker, private, seeding
        local contentpath="$(get_random_content "$contentdir")"
        contentpath="$(tag_contentpath "$contentpath" PRIVATE)"
        contentpath="$(seed_torrent "$contentpath" "$downloaddir")"
        create_torrent \
            "$contentpath" \
            "$torrentdir/$(basename "$contentpath").torrent"\
            "$(get_random_trackers 1)" \
            'private'
    ) &

    (
        # 2 trackers, private, seeding
        local contentpath="$(get_random_content "$contentdir")"
        contentpath="$(tag_contentpath "$contentpath" PRIVATE)"
        contentpath="$(seed_torrent "$contentpath" "$downloaddir")"
        create_torrent \
            "$contentpath" \
            "$torrentdir/$(basename "$contentpath").torrent"\
            "$(get_random_trackers 2)" \
            'private'
    ) &

    # Wait for background processes to finish
    wait
}


create_random_or_varied_torrents() {
    local torrentdir="$1"
    local contentdir="$2"
    local downloaddir="$3"
    # count is true/false or an integer
    local count="$4"

    if [ "$count" = 'true' ]; then
        # Create fixed number of varied torrents
        local created_torrents="$(create_torrents "$torrentdir" "$contentdir" "$downloaddir")"

    elif [ "$count" != 'false' ]; then
        if [ "$count" -gt 0 ]; then
            # Create fixed number of random torrents
            local created_torrents="$(create_random_torrents "$torrentdir" "$contentdir" "$downloaddir" "$count")"
        else
            # Create random number of random torrents
            local created_torrents="$(create_random_torrents "$torrentdir" "$contentdir" "$downloaddir")"
        fi
    else
        local created_torrents=''
    fi

    echo "$created_torrents"
}

create_and_add_torrents_via_watchdir() {
    local torrentdir="$1"
    local contentdir="$2"
    local downloaddir="$3"
    local count="$4"
    local watchdir="$5"

    mkdir -p "$watchdir"

    created_torrents="$(create_random_or_varied_torrents "$torrentdir" "$contentdir" "$downloaddir" "$count")"

    for torrentpath in $created_torrents; do
        add_torrent_via_watchdir "$torrentpath" "$watchdir"
    done
}


create_and_add_torrents_via_rpc() {
    local torrentdir="$1"
    local contentdir="$2"
    local downloaddir="$3"
    local count="$4"
    local clientname="$5"
    local clienturl="$6"

    created_torrents="$(create_random_or_varied_torrents "$torrentdir" "$contentdir" "$downloaddir" "$count")"

    for torrentpath in $created_torrents; do
        add_torrent_via_rpc "$torrentpath" "$downloaddir" "$clientname" "$clienturl"
    done
}
