from .utils import constant

NOT_IMPLEMENTED = constant.get('NOT_IMPLEMENTED', 'N/A')
"""Placeholder return value to indicate missing feature"""

MAX_TORRENT_SIZE = constant.get('MAX_TORRENT_SIZE', 10 * 2**20)   # 10 MiB
"""
Maximum acceptable torrent size

Attempting to add, download, etc a torrent file that is larger than this results
in an error.
"""

HTTP_REQUEST_TIMEOUT = constant.get('HTTP_REQUEST_TIMEOUT', 60.0)
"""
Timeout for generic requests, e.g. when downloading a torrent file

API requests use different timeouts that are specific to each client.
"""
