import asyncio
import collections

import logging  # isort:skip
_log = logging.getLogger(__name__)


class TorrentList(collections.abc.Sequence):
    """
    Immutable sequence of :class:`~.Torrent` objects

    :param Torrent: :class:`~.TorrentBase` instance (usually a return value from
        :func:`~.Torrent_factory`)
    """

    def __init__(self, Torrent):
        self._Torrent = Torrent
        self._torrents = {}

    def __getitem__(self, infohash):
        return self._torrents[infohash]

    def __iter__(self):
        return iter(self._torrents.values())

    def __len__(self):
        return len(self._torrents)

    async def update(self, raw_torrents, attributes):
        """
        ...
        """
        import time
        start = time.time()

        # Remove torrents from internal list that no longer exist
        known_infohashes = set(self._torrents)
        removed_infohashes = known_infohashes.difference(raw_torrents)
        for infohash in removed_infohashes:
            del self._torrents[infohash]

        for infohash, raw in raw_torrents.items():
            torrent = self._torrents.get(infohash, None)
            if torrent:
                # Update existing torrent
                torrent.update(raw, attributes)
            else:
                # Add new torrent
                self._torrents[infohash] = self._Torrent(raw)

            # Relinquish control back to the loop in case another coroutine
            # needs to handle stuff like keyboard input. This increases CPU
            # load, but it should keep a UI from locking up the whole
            # application during the update() call.
            #
            # TODO: Test if this is actually working as expected when there's
            #       UI. Maybe we should call sleep(0) after each attribute? But
            #       that would be very CPU intensive.
            await asyncio.sleep(0)

        time_diff = (time.time() - start) * 1000
        _log.debug('Updated %d cached with %d new torrents in %.3fms',
                   len(self._torrents), len(raw_torrents), time_diff)

    def filter(self, func):
        """
        Return new :class:`~.TorrentList` of matching torrents

        :param func: :class:`callable` that takes a :class:`~.Torrent` object
            and returns whether it is included in the returned list or not

        .. note:: The returned :class:`~.TorrentBase` objects are identical to
                  the ones in the original list. E.g. they are updated when
                  :meth:`update` is called on the either list.
        """
        filtered = type(self)(self._Torrent)
        filtered._torrents = {
            infohash: torrent
            for infohash, torrent in self._torrents.items()
            if func(torrent)
        }
        return filtered

    def __repr__(self):
        tlist = (
            ', '.join('#' + str(ih) for ih in sorted(self._torrents))
            if self._torrents else
            '(empty)'
        )
        return f'<{type(self).__name__} {tlist}>'
