import collections
import functools

import logging  # isort:skip
_log = logging.getLogger(__name__)


def Torrent_factory(name, attributes):
    """
    """
    clsname = f'{name.capitalize()}Torrent'
    bases = (TorrentBase,)
    namespace = {
        '__module__': __name__,
        '__qualname__': f'{__name__}.Torrent_factory({name!r})',
        '__doc__': f'Cached torrent information ({name})',
        'attributes': attributes,
    }

    for attr_cls in attributes:
        namespace[attr_cls.name] = functools.cached_property(
            _get_value_wrapper_factory(attr_cls)
        )

    return type(clsname, bases, namespace)


def _get_value_wrapper_factory(attr_cls):
    """
    Simple wrapper that passes the raw torrent (which is attached to to the
    Torrent class) to :meth:`.attrs.TorrentAttribute.get_value`
    """
    # Define `attr_cls` in get_value()'s namespace for faster access and to make
    # sure get_value() is not getting the last `attr_cls` in the ``for attr_cls
    # in attributes`` loop above.
    def get_value_wrapper(self, attr_cls=attr_cls):
        return attr_cls.get_value(self._raw)

    # Find docstring in parent classes (this should allow cached_property to
    # document the attribute)
    for cls in attr_cls.__mro__:
        if cls.__doc__:
            get_value_wrapper.__doc__ = cls.__doc__
            break

    return get_value_wrapper


class TorrentBase(collections.abc.Mapping):
    """
    Base class for all Torrent classes

    Do not subclass this class directly. Use :func:`Torrent_factory` instead.
    """

    attributes = NotImplemented
    """
    All existing :class:`~.TorrentAttributes`

    This class attribute is set by :func:`~.Torrent_factory`.
    """

    def __init__(self, raw_torrent):
        self._raw = self._raw_old = raw_torrent

    def __getitem__(self, name):
        try:
            return getattr(self, name)
        except AttributeError as e:
            # Don't report our own bugs as KeyErrors!
            if repr(name) in str(e):
                raise KeyError(name)
            else:
                raise e

    def __iter__(self):
        return iter(self.attributes.names)

    def __len__(self):
        return len(self.attributes)

    def __eq__(self, other):
        if isinstance(other, type(self)):
            return other.infohash == self.infohash
        else:
            return NotImplemented

    def __hash__(self):
        return hash(self.infohash)

    def __repr__(self):
        # Only infohash attribute is guaranteed to exist
        try:
            name = self.name
        except AttributeError:
            return f'<{type(self).__name__} {self.infohash}>'
        else:
            return f'<{type(self).__name__} {self.infohash} {name!r}>'

    def update(self, raw_new, attributes):
        """
        Store new raw torrent info and invalidate torrent attributes

        :param dict raw_new: New raw torrent info

        :param attributes: Sequence of :class:`~.TorrentAttributes` to
            invalidate

            .. warning:: All attributes' :attr:`~.TorrentAttribute.dependencies`
                         must be included in `raw_new` and `raw_old`.
        """
        raw_old = self._raw_old
        self._raw_old = self._raw = raw_new

        for attr in attributes:
            # Avoid making expensive get_value() calls by diffing raw
            # dependencies. A change in the raw data does not necessarily mean
            # our value changed, but if the raw data doesn't change, our value
            # definitely hasn't changed.
            if any(
                    raw_new[dep] != raw_old[dep]
                    for dep in attr.dependencies
            ):
                # functools.cached_property uses self.__dict__ as cache. We can
                # simply delete the instance attribute and the new value will be
                # computed when the attribute is accessed.
                try:
                    delattr(self, attr.name)
                except AttributeError:
                    # Attribute wasn't accessed and cached yet
                    pass
