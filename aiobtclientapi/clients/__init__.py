"""
Clients
"""

# isort:skip_file

from .base import APIBase
from .deluge import DelugeAPI
from .qbittorrent import QbittorrentAPI
from .rtorrent import RtorrentAPI
from .transmission import TransmissionAPI
