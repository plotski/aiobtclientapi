from ... import torrentdata
from . import rpc_constants

import logging  # isort:skip
_log = logging.getLogger(__name__)


class Infohash(torrentdata.attrs.Infohash):
    dependencies = (
        'hash',
    )

    def _get_value(raw):
        return raw['hash']


class IsConnected(torrentdata.attrs.IsConnected):
    dependencies = (
        'num_peers',
    )

    def _get_value(raw):
        return raw['num_peers'] > 0


class IsDownloading(torrentdata.attrs.IsDownloading):
    dependencies = (
        'download_payload_rate',
    )

    def _get_value(raw):
        return raw['download_payload_rate'] > 0


class IsStopped(torrentdata.attrs.IsStopped):
    dependencies = (
        'state',
    )

    def _get_value(raw):
        return raw['state'] == 'Paused'


class IsUploading(torrentdata.attrs.IsUploading):
    dependencies = (
        'upload_payload_rate',
    )

    def _get_value(raw):
        return raw['upload_payload_rate'] > 0


class IsVerifying(torrentdata.attrs.IsVerifying):
    dependencies = (
        'state',
    )

    def _get_value(raw):
        return raw['state'] in ('Checking', 'Queued')


class IsIdle(torrentdata.attrs.IsIdle):
    dependencies = torrentdata.attrs.join_dependencies(
        IsStopped,
        IsDownloading,
        IsUploading,
        IsConnected,
        # IsDiscovering,  # Not implemented
    )

    def _get_value(raw):
        return (
            not IsStopped.get_value(raw)
            and not IsDownloading.get_value(raw)
            and not IsUploading.get_value(raw)
            and not IsConnected.get_value(raw)
            # and not IsDiscovering.get_value(raw)  # Not implemented
        )


class IsSeeding(torrentdata.attrs.IsSeeding):
    dependencies = torrentdata.attrs.join_dependencies(
        'progress',
        IsStopped,
        IsVerifying,
    )

    def _get_value(raw):
        return (
            raw['progress'] >= 1.0
            and not IsStopped.get_value(raw)
            and not IsVerifying.get_value(raw)
        )


class Name(torrentdata.attrs.Name):
    dependencies = (
        'name',
    )

    def _get_value(raw):
        return raw['name']


class PeersSeeding(torrentdata.attrs.PeersSeeding):
    dependencies = (
        'total_seeds',
    )

    def _get_value(raw):
        seeds = raw['total_seeds']
        if seeds == rpc_constants.UNKNOWN_SEEDS_COUNT:
            return torrentdata.types.Count.UNKNOWN
        else:
            return seeds
